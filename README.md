# Splat

Splat is a macro processor inspired by escape-scheme, lips and guile-lips. Its main advantage over those is its support for `@func(a1 a2 a3)` syntax, which is preferable in this context (IMHO). Note that this does not remove the `@(func a1 a2 a3)` syntax, but expands upon it (`@(lambda ...)(a1 a2 a3)` is also valid).

Unlike lips (which supports a similar syntax), it does not require a call to an external procedure to read arguments and it uses Guile.

It is meant to be used in any file, not just Scheme code.

Here is an example:

```
Hello world!

2 + 2 = @(+ 2 2)

@(define (fact n)
  (case n
    ((0 1) 1)
    (else (* n (fact (- n 1))))))

Factorial definition:

0! = 1
1! = 1
n! = n * (n-1)!

10! is @fact(10)
```

# Usage

`splat < input > output`

